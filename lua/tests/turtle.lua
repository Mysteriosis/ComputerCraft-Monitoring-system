--Variables
local url = "https://ccdimmonitor.herokuapp.com/api/signals/"
local signalSide = {"front", "left", "right"}

local run = true
local computerID = nil

--Prompting
function prompt()
  while run do
    term.write("Command >")
    cmd = read()

    if cmd == "stop" then
      print("Shutdown program...")
      print("Bye.")
      run = false --Kills coroutine => stops program.
    else
      print("Commande inconnue.")
      print("Commandes disponibles: stop.")
    end
  end
end

--Signals
function signals()
  while run do
    local req = http.get(url)
    -- if ... then
    --   redstone.setOutput(signalSide, true)
    -- end
  end
end

--Main
while computerID == nil do
  term.write("ID de l'ordinateur serveur: ")
  local id = read()
  if tonumber(id) ~= nil then
    computerID = id 
  else
    print(" -> ID incorrect.")
  end
end

print("Turtle #" .. os.getComputerID() .. " connectee a l'ordinateur #" .. computerID .. "  active.")
parallel.waitForAll(signals, prompt) --Coroutines, Yay !