--Variables
name = "cofh_thermalexpansion_energycell_"
first_id = nil
last_id = nil
nb_cell = 0

stat = 0
limits = {0, 14, 49}
status = {"DANGER", "ATTENTION", "BON"}
statsColors = {colors.red, colors.yellow, colors.green}

active = false
run = true

-- tank1 = peripheral.wrap(valveName1)
-- tank2 = peripheral.wrap(valveName2)
mon = peripheral.wrap("bottom")
mon.setTextScale(1)

--Shallow Clone, but follow arrays
function clone(original)
  local orig_type = type(original)
  local copy
  if orig_type == 'table' then
    copy = {}
    for k, v in pairs(original) do
      if type(v) == 'table' then
        copy[k] = clone(v)
      else
        copy[k] = v
      end
    end
  else -- number, string, boolean, etc
    copy = original
  end
  return copy
end

-- --Extract tank infos
-- function getTank(tankPeriph)

--     local tableInfo = tankPeriph.getTankInfo("unknown") -- Local to the getTank function.
 
--     for k,v in pairs(tableInfo) do
--         fluidRaw = v.rawName
--         fluidName = v.name
--         fluidAmount = v.amount
--         fluidCapacity = v.capacity
--     end

--     -- Returning the values of global variables (which are nil).
--     return {["fluidRaw"] = fluidRaw, ["fluidName"] = fluidName, ["fluidAmount"] = fluidAmount, ["fluidCapacity"] = fluidCapacity}
-- end

--Commands to motors
function toggleMotors()
  if active then
    active = false
    redstone.setOutput("left", active)
    print("Moteurs: OFF")
  else
    active = true
    redstone.setOutput("left", active)
    print("Moteurs: ON")
  end
end

--Monitoring
function monitor()
  local cells = {}
--   local previousTanks = nil
--   local previousBothTanks = nil
--   local temps = "- ? -"
  
  while run do
--     mon.clear()
--     mon.setCursorPos(1,1)

--     tanks[0] = getTank(tank1)
--     tanks[1] = getTank(tank2)

--     --Left Part
--     if tanks[0].fluidName then
--       tanks[0].fluidName = string.gsub(tanks[0].fluidName, "^%l", string.upper)
--       tanks[0]["cap"] = tanks[0].fluidCapacity / 1000
--       tanks[0]["percentfull"] = 0
     
--       if tanks[0].fluidAmount ~= nil then
--         -- Use math.floor to convert to integers
--         tanks[0].fluidAmount = math.floor(tanks[0].fluidAmount / 1000)
--         tanks[0].percentfull = math.floor(100 * tanks[0].fluidAmount / tanks[0].cap)
--       end

--       mon.write(" -Cuve gauche-")
--       mon.setCursorPos(1,2)
--       mon.write("Contient: "..tanks[0].fluidName)
--       mon.setCursorPos(1,3)
--       mon.write("Quantite: "..tanks[0].percentfull .."%")

--       if previousTanks ~= nil then
--         outflow = tanks[0].fluidAmount - previousTanks[0].fluidAmount
--         mon.setCursorPos(1,4)
--         mon.write("Debit: ")

--         if outflow >= 0 then
--           mon.setTextColor(colors.green)
--           if outflow == 0 then
--             mon.write("aucun")
--           else
--             mon.write("+"..outflow.." b/s")
--           end
--           mon.setTextColor(colors.white)
--         else
--           mon.setTextColor(colors.red)
--           mon.write(outflow.." b/s")
--           mon.setTextColor(colors.white)
--         end
--       end
--     end

--     --Right Part
--     if tanks[1].fluidName then
--       tanks[1].fluidName = string.gsub(tanks[1].fluidName, "^%l", string.upper)
--       tanks[1]["cap"] = tanks[1].fluidCapacity / 1000
--       tanks[1]["percentfull"] = 0
     
--       if tanks[1].fluidAmount ~= nil then
--         -- Use math.floor to convert to integers
--         tanks[1].fluidAmount = math.floor(tanks[1].fluidAmount / 1000)
--         tanks[1].percentfull = math.floor(100 * tanks[1].fluidAmount / tanks[1].cap)
--       end

--       mon.setCursorPos(16,1)
--       mon.write(" -Cuve droite-")
--       mon.setCursorPos(16,2)
--       mon.write("Contient: "..tanks[1].fluidName)
--       mon.setCursorPos(16,3)
--       mon.write("Quantite: "..tanks[1].percentfull .."%")

--       if previousTanks ~= nil then
--         local outflow = tanks[1].fluidAmount - previousTanks[1].fluidAmount
--         mon.setCursorPos(16,4)
--         mon.write("Debit: ")

--         if outflow >= 0 then
--           mon.setTextColor(colors.green)
--           if outflow == 0 then
--             mon.write("aucun")
--           else
--             mon.write("+"..outflow.." b/s")
--           end
--           mon.setTextColor(colors.white)
--         else
--           mon.setTextColor(colors.red)
--           mon.write(outflow.." b/s")
--           mon.setTextColor(colors.white)
--         end
--       end
--     end

--     --Separation
--     mon.setCursorPos(5,6)
--     mon.write("--------------------")

--     --Bottom Part
--     if tanks[0].fluidName and tanks[1].fluidName and tanks[0].fluidName == tanks[1].fluidName then
--       bothTanks = {
--         ["fluidAmount"] = tanks[0].fluidAmount + tanks[1].fluidAmount,
--         ["fluidCapacity"] = tanks[0].fluidCapacity + tanks[1].fluidCapacity,
--         ["cap"] = tanks[0].cap + tanks[1].cap
--       }

--       if bothTanks.fluidAmount ~= nil then
--         -- Use math.floor to convert to integers
--         bothTanks.percentfull = math.floor(100 * bothTanks.fluidAmount / bothTanks.cap)
--       else
--         bothTanks.percentfull = 0
--       end

--       mon.setCursorPos(1,7)
--       mon.write("   Informations generales")
--       mon.setCursorPos(1,9)
--       mon.write("Quantite: "..bothTanks.percentfull .."% - ")

--       for k,v in pairs(limits) do
--         if bothTanks.percentfull >= v then
--           stat = tonumber(k)
--         end
--       end

--       mon.setTextColor(statsColors[stat])
--       mon.write(status[stat])
--       mon.setTextColor(colors.white)

--       if previousTanks ~= nil then
--         local outflow = bothTanks.fluidAmount - previousBothTanks.fluidAmount
--         mon.setCursorPos(1,10)
--         mon.write("Debit: ")

--         if outflow >= 0 then
--           mon.setTextColor(colors.green)
--           if outflow == 0 then
--             mon.write("aucun")
--           else
--             mon.write("+"..outflow.." b/s")
--           end
--           mon.setTextColor(colors.white)
--         else
--           mon.setTextColor(colors.red)
--           mon.write(outflow.." b/s")
--           mon.setTextColor(colors.white)
--         end

--         mon.setCursorPos(1,11)
--         mon.write("Vide dans environ: ")

--         if outflow < 0 then
--           local deb = outflow * -1
--           local sec = bothTanks.fluidAmount / deb
--           local minutes = math.floor(sec/60)
--           local heures = math.floor(minutes/60)
--           sec = sec%60
--           minutes = minutes%60

--           if sec < 10 then sec = "0"..sec end
--           if minutes < 10 then minutes = "0"..minutes end

--           temps = heures .. ":" .. minutes .. ":" .. sec
--         elseif outflow > 0 then
--           temps = "- ? -"
--         end

--         --Extraction
--         if bothTanks.percentfull <= 25 and outflow < 0 then
--           if not active then toggleExtract() end
--         elseif bothTanks.percentfull == 100 then
--           if active then toggleExtract() end
--         end

--         mon.write(temps)
--       end

--       previousBothTanks = clone(bothTanks)
--     end

--     previousTanks = clone(tanks)
    sleep(1) --refresh every 1 sec ! change if server lags
  end
end

--Prompting
function prompt()
  while run do
    print("Command >")
    cmd = read()

    if cmd == "toggle" then
      toggleExtract()
    elseif cmd == "stop" then
      print("Shutdown program...")
      print("Bye.")
      run = false --Kills coroutine => stops program.
    else
      print("Commande inconnue.")
      print("Commandes disponibles: toggle, stop.")
    end
  end
end

--Main
print("Demarrage du monitoring...")

while tonumber(first_id) == nil do 
  print("ID de depart:")
  first_id = read()
end

while tonumber(last_id) == nil do
  print("ID de fin:")
  last_id = read()
end

nb_cell = last_id - first_id + 1

term.clear()

print("Monitoring energetique actif pour " .. nb_cell .. " cellule(s) sur l'ordinateur #" .. os.getComputerID() .. ".")
parallel.waitForAll(monitor, prompt) --Coroutines, Yay !
mon.clear()