# ComputerCraft-Monitoring-system
***ComputerCraft's scripts used to monitoring IronTanks & EnergyCells in Minecraft 1.6.4.***

## Dependencies
Minecraft 1.6.4 + Forge  
- [Computercraft](http://www.computercraft.info/)
- [OpenPeripheral](http://openmods.info/)
- [RailCraft](http://www.railcraft.info/)
- [ThermalExpansion](http://teamcofh.com/)